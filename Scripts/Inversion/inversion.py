import numpy as np
from oblate_tools.ccf import CCFInterpolant
from oblate_tools.inversion import CachedOblateSpheroid, InversionProblem

class EXPRES_TESS_Inversion:

    def __init__(self, datafile, ccf_file, N_θ, N_φ,
                 limb_darkening=None, **prob_kwargs):

        self.data = np.load(datafile, allow_pickle=True)[()]
        if limb_darkening is not None:
            self.limb_darkening = limb_darkening
        else:
            self.limb_darkening = lambda x: np.where(x > 0, x, 0)

        self.mesh = CachedOblateSpheroid.ones(N_θ, N_φ, self.data['ε'])
        self.ccf = CCFInterpolant(ccf_file)

        kwargs = {k: self.data[k] for k in ['flux', 'ccfs', 'e_ccfs']}
        kwargs['v_grids'] = self.data['vs']
        kwargs['σ_flux'] = self.data['e_flux']
        kwargs['ψ_ccf'] = self.data['ψ_expres']
        kwargs['ψ_flux'] = self.data['ψ_lc']

        self.prob = InversionProblem(self.mesh, self.data['i0'], self.limb_darkening,
                                     **{**kwargs, **prob_kwargs})
        self.q = None
        self.sol = None

    def do_phot(self, y0_trial=1, λ_tikhonov=.1, conditioning_factor=1e-3, σ0=0, maxiter=100, **kwargs):

        self.prob.λ_tikhonov = λ_tikhonov * conditioning_factor
        self.prob.f_flux = 1/len(self.data['ψ_lc']) * conditioning_factor
        self.prob.f_ccf = 0

        mesh = self.mesh * 0 + 1
        phot_scale = mesh.project(0, self.data['i0'],
                                  limb_darkening=self.limb_darkening)
        mesh = self.mesh * 0 + (y0_trial / phot_scale)

        init = mesh + np.random.normal(scale=σ0 * y0_trial / phot_scale, size=mesh.shape)
        self.q = self.prob(init, maxiter=maxiter, options={**dict(gtol=1e-6), **kwargs})
        self.sol = self.prob._(self.q['x'])

    def do_joint(self, Teff, clear=False, λ_tikhonov=.1, λ_ccf_scale=0, init=None, σ0=0, **kwargs):

        if clear:
            self.prob._ccf_model = {}

        assert self.sol is not None, "Must have run photometric inversion first"
        y0 = self.sol.surface_integrate() / self.sol.S
        T0 = Teff
        inter = self.ccf
        veq = self.data['vsini'] * 1e5 / np.sin(self.data['i0'])

        def ccf_model(L, y, v, Δv, dy=0):
            T = (L * y / y0) ** (1/4) * T0
            if dy == 0:
                return inter(v * veq, T)
            elif dy == 1:
                return inter(v * veq, T, dy=1) * T0 * (L / y0 / y**3)**(1/4) / 4

        self.prob.mesh.ccf = ccf_model
        self.prob.model_ccf = ccf_model

        self.prob.λ_tikhonov = λ_tikhonov / ((self.sol**2).surface_integrate())
        self.prob.f_ccf = 1 / len(self.data['ψ_expres']) 
        self.prob.f_flux = 1 / len(self.data['ψ_lc'])
        self.prob.f_ccf_scale = λ_ccf_scale

        # normalise CCF scales

        masks = []
        for _ in self.data['vs']:
            m = np.ones_like(_, dtype=bool)
            m[20:-20] = False
            masks.append(m)

        if init is None:
            init = self.sol + np.random.normal(scale=σ0, size=self.mesh.shape)
        else:
            init = self.mesh.like(init)

        self.prob.renorm_ccf(init, slices=masks)
        self.q = self.prob(init, **{**dict(method='l-bfgs-b', maxiter=100), **kwargs})

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Sigma Gem Inversion: Grid Parameters")
    parser.add_argument("y0", type=float, help="Normalisation to use for initial guess in photometry")
    parser.add_argument("Teff", type=float, help="Effective Temperature")
    parser.add_argument("out", type=str, help="Output filename")
    parser.add_argument("--tikh", type=float, default=.1, help="λ_tikh for regularisation")
    args = parser.parse_args()

    from oblate_tools.science import limb_darken_van_Hamme_93
    limb_darken_σ_Gem = limb_darken_van_Hamme_93(0.767, 0.059)

    inversion = EXPRES_TESS_Inversion("σ_Gem.npy", "PHOENIX_2.50_0.00_K2.npy",
                                      90, 320, limb_darkening=limb_darken_σ_Gem, vscale=1)

    inversion.do_phot(args.y0)
    phot = inversion.q

    inversion.do_joint(args.Teff, λ_tikhonov=args.tikh)
    np.save(args.out, {
        'phot': phot, 'combined': inversion.q, 'costs': inversion.prob.costs
        })
