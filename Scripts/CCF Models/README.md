# CCF Models from PHOENIX Spectra

This notebook (and associated shell script) constructs CCF interpolants for use with `oblate_tools.ccf.CCFInterpolant`. To use these, you will need to
have the EXPRES pipeline installed (`github/jmbrewer/expres_pipeline`), including the CCF code (which requires compilation via `fpp`/`gfortran`). Once it is set up, do the following:

1. Make a folder `expres` and populate it with one reference reduced FITS file (as produced by the EXPRES pipeline).
2. Make a symlink to the appropriate ESPRESSO line mask.
3. Modify `prep.sh` to point at the correct values of [Fe/H] and log g that you need for your model.
4. Make a folder `phoenix`. `cd` into it and `source ../prep.sh` to download the PHOENIX model spectra. You will also need to download the wavelength solution file (not included).
5. Run the notebook with appropriate modifications (specifying ESPRESSO linemask, log g, and [Fe/H] of the target).

This will produce a `.npy` file that can be supplied as an argument to `CCFInterpolant`.
