from setuptools import setup, find_packages
setup(
    name="oblate_tools",
    version="0.3",
    packages=find_packages(),
    install_requires=['pyglet==1.5', 'pygarrayimage', 'numpy', 'scipy',
                      'matplotlib'],

    # metadata for upload to PyPI
    author="Joel Ong",
    author_email="joel.ong@yale.edu",
    description="Tools for manipulating and viewing functions on oblate spheroids",
    license="GPLv3"
)
