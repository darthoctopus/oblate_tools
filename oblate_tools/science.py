#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Collection of empirical relations pertaining to stars & doppler imaging
'''

import numpy as np

def limb_darken_power(c=None):
    r'''
    Expansion of limb-darkening profile in powers of (1 - μ),
    of the form I/I0 = 1 - \sum_j c_j(1 - μ)^j.

    See Kopal 1950, Kipping 2013, Maxted & Gill 2018.
    The most commonly-used variant is quadratic.

    c contains polynomial coefficients in *ascending* order.

    e.g. if we want 1 - a1 * (1 - μ) - a2 * (1 - μ)^2,
    we would supply [a1, a2].

    The default coefficients are from Neckel and Labs (1994).

    Kopal 1950 provides c=[0.6046, 0.0189, -0.0002, 0.0145].
    Neckel and Labs (1994) provide c=[0.55453, 0.08018, 0.14604, -0.23476, 0.21706]
    for the sun at 559 nm.
    Cox (2000) provides c=[0.47, 0.23], describing
    an empirical fit to the Sun at 550 nm.
    '''

    if c is None:
        c = [0.55453, 0.08018, 0.14604, -0.23476, 0.21706]

    def _(μ):
        α = (1 - μ)
        return np.where((μ >= 0) & (μ <= 1), 1 - np.polyval(c[::-1], α) * α, 0)
    return _

def limb_darken_power_2(c=0.86, α=0.68):
    '''
    Power-2 limb-darkening profile, works better for cool stars.

    See Hestroffer 1997, Morello+ 2017, Maxted & Gill 2018.

    Parameters are provided from Hestroffer 1997, fitted to the sun
    for μ >= 0.02.
    '''

    def _(μ):
        return np.where((μ >= 0) & (μ <= 1), 1 - c * (1 - np.power(np.abs(μ), α)), 0)
    return _

def limb_darken_van_Hamme_93(x_λ, y_λ):
    '''
    Limb darkening law of the form of van Hamme (1993).

    This returns a callable limb-darkening law when coefficients are supplied.
    '''
    def _(μ):
        return np.where((μ >= 0) & (μ <= 1), 1 - x_λ * (1 - μ) - y_λ * (1 - np.sqrt(μ)), 0)
    return _

def solar_Ω_profile(θ, α=None):
    r'''
    (approximate) solar surface differential rotation profile,
    hopefully a good fit for cool stars.

    We accept Ω_eq and coefficients α so that
    Ω = Ω_eq (1  - \sum_j α_j sin^2j λ), where λ is the latitude.
    If there is only one coefficient, α has the usual interpretation of
    being ΔΩ/Ω_eq.

    See Hackman+ 2018 for a description of *one* pipeline.

    Default values are from Snodgrass and Ulrich (1990).
    For the sun, Ωeq = 2.9721e-6 s^-1.
    '''
    if α is None:
        α = [0.16285, 0.1215]
    z = 1 - np.sin(θ)**2 # === sin^2 λ
    return 1 - z * np.polyval(α[::-1], z)

def constant_Ω_profile(θ):
    r'''
    Constant rotational profile
    '''

    return np.ones_like(θ)

def kent(θ, φ, θ0, φ0, κ=1):
    '''
    Kent distribution: generalisation of the Gaussian distribution to S2.
    Parameters:

    θ, φ: coordinates on S2
    θ0, φ0: coordinates of central point S2
    κ: width parameter (-> R^2/σ^2)
    '''

    cosψ = np.cos(θ)*np.cos(θ0) + np.sin(θ)*np.sin(θ0)*np.cos(φ-φ0)
    exponent = κ * cosψ
    prefactor = κ/(4*np.pi) / np.sinh(κ)

    return prefactor * np.exp(exponent)
