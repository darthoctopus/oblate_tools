#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''OpenGL visualisation tool for the OblateSpheroid class'''

import numpy as np
from matplotlib import cm
from pyglet.gl import *
from pyglet.extlibs import png
from pygarrayimage.arrayimage import ArrayInterfaceImage
from .oblate_spheroid import OblateSpheroid, μ, identity

def normalise(x, vmin=None, vmax=None):
    '''
    Normalise to range (0, 1)
    '''
    y = np.real(x[...])
    if vmin is None:
        vmin = np.nanmin(y)
    if vmax is None:
        vmax = np.nanmax(y)
    return np.minimum(np.maximum((y - vmin) / (vmax - vmin), 0), 1)

def round(x, bits=8, cmap=None, **kwargs):
    '''
    Safe rounding and casting to 8 bits for use with opengl textures
    '''
    if np.allclose(x, np.mean(x)):
        normalised = np.ones_like(x[...]) / 2.
    else:
        normalised = normalise(x, **kwargs)
    if cmap is None:
        return np.round(normalised * (2**bits - 1)).astype(f'uint{max(8, bits)}')

    return np.round(vars(cm)[cmap](normalised) * (2**bits - 1)).astype(f'uint{max(8, bits)}')

WHITE_TEXT = {
    'font_name': 'Consolas',
    'color': (255, 255, 255, 255),
    'font_size': 12,
    'anchor_x': 'left'
}
MIN_SIZE = (640, 480)
MAX_SIZE = (2**16, 2**16)

class RendererWindow(pyglet.window.Window):

    '''
    Rendering window class: shows the oblate spheroid (interactively)
    as well as diagnostic information. Press A to toggle rotation, and press R to reset
    to original orientation.

    Accepts an OblateSpheroid as input. i0 and φ0 set the initial orientation of the
    viewer (i.e. they will be what the view resets to when R is pressed).
    '''

    def __init__(self, spheroid, *args, i0=0, φ0=0, darken=False, limb_darkening=identity,
                 vmin=None, vmax=None,
                 animate=False, rotate=False, displacement=False, cmap=None, save=None, **kwargs):
        '''
        Initialise the renderer
        '''
        assert isinstance(spheroid, OblateSpheroid), "Must supply an OblateSpheroid"
        self.φ0 = φ0
        self.i0 = i0

        self.φ = φ0
        self.i = i0

        self.params = {}
        self.labels = {}
        self.texture_data = {}
        self.textures = {}
        self.meshes = {}

        self.vmin = vmin
        self.vmax = vmax


        self.params['rotate'] = rotate
        self.params['animate'] = animate
        self.params['displacement'] = displacement
        self.params['scale'] = 1
        self.params['save'] = save
        self.data = spheroid
        self.cmap = cmap
        self.texture_data['data'] = round(self.data, cmap=self.cmap,
                                          vmin=self.vmin, vmax=self.vmax)

        # set up spheroid

        self.projected_integral = 0
        self.params['darken'] = darken
        self.darkener = limb_darkening

        self.labels['status'] = pyglet.text.Label('', **WHITE_TEXT, x=12, y=0,
                                                  multiline=True, width=2560,
                                                  anchor_y='top')
        self.labels['min'] = pyglet.text.Label(f'{np.nanmin(np.real(spheroid)):.2f}',
                                               **WHITE_TEXT, x=2, y=0,
                                               anchor_y='bottom')
        self.labels['max'] = pyglet.text.Label(f'{np.nanmax(np.real(spheroid)):.2f}',
                                               **WHITE_TEXT, x=2, y=276,
                                               anchor_y='bottom')
        super().__init__(*args, **kwargs)
        self.labels['status'].y = self.height - 10
        self.textures['data'] = ArrayInterfaceImage(self.texture_data['data'])
        self.meshes['sphere'] = self.generate_vertices(spheroid, displacement=displacement)

        # set up colorbar

        r = np.linspace(0, 1, 256)[:,None] * np.ones((1, 2))
        self.textures['cbar'] = ArrayInterfaceImage(round(r, cmap=self.cmap))
        self.meshes['cbar'] = pyglet.graphics.vertex_list(4,
                                                          ('v2f',
                                                           [0, 20, 20, 20, 0, 276, 20, 276]),
                                                          ('t2f', [0, 0, 1, 0, 0, 1, 1, 1]))

        self.set_caption(f'Oblate spheroid viewer: ε = {self.data.ε:.2f}')
        if self.params['darken']:
            self.redraw()

    @staticmethod
    def generate_vertices(spheroid, displacement=False):
        '''
        Given a numpy array, generate the vertices required to draw the oblate spheroid.
        '''
        vlists = []
        lims = np.array(spheroid.shape) / np.power(2, np.ceil(np.log2(spheroid.shape)))

        r = 1
        if displacement:
            μ = float(spheroid.surface_integrate() / spheroid.S)
            σ = float(np.sqrt(((spheroid - μ)**2).surface_integrate() / spheroid.S))
            if σ != 0:
                r += (lambda x: np.hstack((x, x[:,0][:,None])))((spheroid - μ)/σ/15)

        def vertex(θ, φ, ε):
            '''
            viewport x, -z, y = physical x, y, z.

            Some finagling required to support non-power-of-two textures in compat mode.
            '''
            x = r * np.sin(θ) * np.cos(φ)
            z = -r * np.sin(θ) * np.sin(φ)
            y = r * ε * np.cos(θ) + 0 * φ

            s = φ / (2 * np.pi) + 0 * θ
            t = θ / np.pi + 0 * φ

            return np.array([x, y, z, s * lims[1], t * lims[0]]).transpose(1, 2, 0)

        vertices = vertex(spheroid.θ,
                          np.hstack((spheroid.φ, [[spheroid.φ[0,0] + 2 * np.pi]])), spheroid.ε)

        for i in range(len(vertices) - 1):
            verts = np.concatenate(np.hstack((vertices[i, :, :3], vertices[i+1, :, :3])))
            texc = np.concatenate(np.hstack((vertices[i, :, 3:], vertices[i+1, :, 3:])))
            vlist = pyglet.graphics.vertex_list(len(verts)//3, ('v3f', verts), ('t2f', texc))
            vlists.append(vlist)
        return vlists

    def on_draw(self):
        aspect = self.width / self.height

        glEnable(GL_DEPTH_TEST)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(-aspect, aspect,-1,1,-10,10)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glRotatef(int((np.pi/2 - self.i) / (2 * np.pi) * 360), 1, 0, 0)
        glRotatef(int(self.φ / (2 * np.pi) * 360), 0, 1, 0)
        glColor3f(1,1,1)
        glScalef(self.params['scale'], self.params['scale'], self.params['scale'])
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, self.textures['data'].get_texture().id)
        for v in self.meshes['sphere']:
            v.draw(GL_TRIANGLE_STRIP)
        glDisable(GL_DEPTH_TEST)

        # draw HUD
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluOrtho2D(0, self.width, 0, self.height)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        for k in ['status', 'min', 'max']:
            self.labels[k].draw()

        # draw cbar

        glBindTexture(GL_TEXTURE_2D, self.textures['cbar'].get_texture().id)
        self.meshes['cbar'].draw(GL_TRIANGLE_STRIP)

        # if screenshot is to be taken, save and quit

        if self.params['save'] is not None:
            pyglet.image.get_buffer_manager().get_color_buffer().save(self.params['save'])
            self.close()


    def on_resize(self, width, height):
        super().on_resize(width, height)
        self.labels['status'].y = self.height - 10
        self.labels['status'].width = self.width

    def update(self, dt):
        '''
        This function is called every frame to update the contents of the GL area
        '''
        if self.params['rotate']:
            self.φ += 2 * np.pi / 360
            if self.params['darken']:
                self.redraw()
        self.φ = self.φ % (2 * np.pi)
        self.labels['status'].text =\
        f'Limb darkening {self.params["darken"]}\nφ = {self.φ:.3f}; i = {self.i:.3f}'
        self.labels['status'].text +=\
        f'\nProjected integral = {self.projected_integral:.3f} (last evaluated)'

    def on_mouse_drag(self, x, y, dx, dy, button, modifiers):
        if button == pyglet.window.mouse.LEFT:
            self.φ = (self.φ + np.pi * dx / self.width) % (2 * np.pi)
            self.i = max(min(self.i + np.pi * dy / self.height, np.pi/2), 0)

    def on_mouse_scroll(self, x, y, dx, dy):
        self.params['scale'] = (self.params['scale'] / 1.1
                                if dy < 0 else self.params['scale'] * 1.1)

    def on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.R:
            # Toggle rotation
            self.params['rotate'] = not self.params['rotate']
        elif symbol == pyglet.window.key.H:
            # Reset view
            self.φ = self.φ0
            self.i = self.i0
            self.params['scale'] = 1
        elif symbol == pyglet.window.key.P:
            # Evaluate projected integral
            self.projected_integral = self.data.project(self.φ, self.i,
                                                        method='quick',
                                                        limb_darkening=self.darkener)
        elif symbol == pyglet.window.key.L:
            self.params['darken'] = not self.params['darken']
            self.redraw()
        elif symbol == pyglet.window.key.D:
            self.params['displacement'] = not self.params['displacement']
            self.meshes['sphere'] = self.generate_vertices(self.data, self.params['displacement'])
        elif symbol == pyglet.window.key.S:
            self.save("Snapshot.png")

    def redraw(self):
        '''
        Upon changing the texture (e.g. when limb darkening is changed),
        force a redraw and update of labels
        '''
        if not self.params['darken']:
            m = self.data
        else:
            cosψ = μ(self.data.θ, self.data.φ + self.φ, self.data.ε, self.i)
            m = self.data * np.where(cosψ > 0, self.darkener(cosψ), 0)
        self.texture_data['data'] = round(m, cmap=self.cmap, vmin=self.vmin, vmax=self.vmax)
        self.labels['min'].text = f'{np.nanmin(np.real(m)):.2f}'
        self.labels['max'].text = f'{np.nanmax(np.real(m)):.2f}'
        self.textures['data'] = ArrayInterfaceImage(self.texture_data['data'])
        # self.textures['data'].dirty()

    def save(self, filename):
        '''
        Save the currently drawn frame buffer to the specified filename
        '''
        save(self.texture_data['data'], filename, bits=8)

def show(sph, width=800, height=600, **kwargs):
    '''
    Convenience wrapper. Call this as render.show(sph)
    '''
    window = RendererWindow(sph, width, height, **kwargs)
    window.set_minimum_size(*MIN_SIZE)
    window.set_maximum_size(*MAX_SIZE)
    pyglet.clock.schedule_interval(window.update,1/60.0)
    pyglet.app.run()

def collapse_axes(data):
    '''
    Collapse an input numpy array to 2D
    '''
    if len(data.shape) == 2:
        return data
    return data.reshape(data.shape[0], -1)

def save(data, filename, bits=8):
    '''
    Export the array to raw PNG (normalised to min/max values).
    May be useful for other purposes.
    '''
    if len(data.shape) == 2:
        format = 'L'
    else:
        assert len(data.shape) == 3 and data.shape[-1] in [1, 2, 3, 4],\
        "Image must have 1, 2, 3 or 4 channels"
        format = ['L', 'LA', 'RGB', 'RGBA'][data.shape[-1] - 1]
    format += f';{bits}'

    png.from_array(collapse_axes(data), format).save(filename)
