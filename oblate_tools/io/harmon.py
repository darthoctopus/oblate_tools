#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Interface between OblateSpheroid class and light curve inversion results
presented by the code of Harmon & Crews 2000, provided by Rachael Roettenbacher,
intended for use with EXPRES CCFs and TESS data.
'''

import numpy as np
from scipy.interpolate import interp1d

from oblate_tools import OblateSpheroid
from oblate_tools.oblate_spheroid import identity

def _(degrees):
    '''
    Degrees to radians
    '''
    return np.array(degrees) / 360 * 2 * np.pi

def read_txt(input, θ, φ, ε=1, **kwargs):
    '''
    Turn output of Rob Harmon's code into an OblateSpheroid object.

    The output files contain rows with three numbers each in the format
    (latitude, longitude, intensity); latitude and longitude are in degrees,
    and intensity is in arbitrary units,

    NOTE: Rachael's longitude convention is of opposite handedness
    to those in our coordinate system.

    Inputs
    ------
    input: Either filename of input file, or numpy array.
    θ: vector of colatitudes θ ∈ [0, π]
    φ: vector of azimuths φ ∈ [0, 2π)
    ε: oblateness parameter b/a ∈ (0, 1], 1 being sphere
    kwargs: Keyword arguments to be passed to `scipy.interpolate.interp1d`.

    Returns an OblateSpheroid of shape (len(θ), len(φ))
    '''

    if isinstance(input, np.ndarray):
        data = input
    elif isinstance(input, str):
        data = np.loadtxt(input)
    else:
        raise NotImplementedError("Unsupported input type")

    # astrophysical coordinate system
    δ, λ, I = data.T

    latitudes = np.unique(δ)

    # viewing coordinate system
    B = np.zeros((len(θ), len(φ)))

    dθ = np.mean(np.diff(_(latitudes)))

    for d in latitudes:

        θ0 = np.pi/2 - _(d)
        m = (δ == d)

        l = λ[m] # longitudes
        dl = np.mean(np.diff(l))
        i = I[m]

        m_θ = np.abs(θ - θ0) <= dθ/2

        if len(l) == 1:
            B[m_θ, :] = float(i)
        else:
            B_ = interp1d(_([l[0]-dl, *l, l[-1]+dl]),
                      [i[-1], *i, i[0]], **kwargs)

            B[m_θ, :] = B_(2 * np.pi -φ)

    return OblateSpheroid(B, θ, φ, ε)

def phase_to_viewport_φ(phase):
    '''
    Turn a phase (between 0 and 1) to a viewport azimuth φ
    (between 0 and 2π).
    '''
    return (1-phase) * 2 * np.pi

def map_to_profile(sph, φ0, i0, v, σ_v=1/50,
                   limb_darkening=identity,
                   Ω=np.ones_like):

    '''
    Turn an OblateSpheroid object into a line profile (in the sense of
    a line-spread function, approximating a δ function).

    NOTE: The minus sign is there for a reason! Rachael's longitude and phase
    conventions are of opposite handedness to those in our coordinate system.
    In particular, this means the velocities are of opposite sign to
    those assumed in the project_velocity() method.

    Inputs
    ------
    sph: OblateSpheroid object
    φ0: Viewport phase angle
    i0: Inclination angle (0 means axis points towards observer)
    v: vector of trial velocities (in the same sense as CCF). Note that this is
       to be specified in units of the equatorial velocity.
    σ_v: softening width to reduce aliasing when computing the projection
    integral; see implementation in OblateSpheroid object for more information.
    limb_darkening: Callable limb_darkening function
    Ω: Callable latitudinal differential-rotation profile

    Returns a numpy array of the same shape as v.
    '''
    return sph.project_velocity(φ0, i0, -v, σ_v, limb_darkening=limb_darkening, Ω=Ω)
