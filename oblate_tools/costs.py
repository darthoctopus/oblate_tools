#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Utilities and cost functions for use with optimisation.

All "gradients" are actually functional derivatives (i.e. gradient
with respect to the value at each grid point).
'''

import numpy as np
from tqdm.auto import tqdm
from scipy.optimize import minimize
from .oblate_spheroid import identity
from .ccf import CCFOblateSpheroid

def tqdm_minimize(*args, tqdm=tqdm, maxiter=100, **kwargs):
    '''
    Wrapper for scipy.optimize.minimize with a tqdm progressbar
    '''

    if 'options' not in kwargs:
        kwargs['options'] = {}

    kwargs['options'] = {**kwargs['options'], 'maxiter': maxiter}

    if 'callback' in kwargs:
        old_callback = kwargs['callback']
        del kwargs['callback']
    else:
        old_callback = None

    with tqdm(total=maxiter) as p:
        def callback(*_):
            if old_callback is not None:
                old_callback(*_)
            p.update()
        q = minimize(*args, callback=callback, **{'method': 'cg', **kwargs})
        if q['success']:
            p.update(maxiter)
    return q

def array_hash(x):
    '''
    Array hash function to allow using arrays as dict keys
    '''
    return hash(np.array(x).data.tobytes())

class LightcurveProblem:
    '''
    Container for light-curve inversion cost functions, to be used as a mixin for
    the actual inversion class.

    Arguments:

    i: (float) inclination in RADIANS
    limb_darkening: (callable) Limb-darkening law as function of μ = cos ψ

    Keyword arguments:
    flux: (ndarray) flux of light curve (units u such that the OblateSpheroid itself
          is in u/sterad or equivalent aspherical quantity)
    σ_flux: (ndarray) observational error associated with light curve. Must be
            same shape as flux.
    ψ_flux: (ndarray) rotational phases associated with flux observations. Must
            be same shape as flux.

    In addition to these arguments, we define a few properties:

    f_flux: relative weight of the lightcurve's contribution to the χ² function
           when used as a mixin

    d_lightcurve: functional derivative of the projection at each ψ_lc with respect
                  to intensity. Since the projection is proportional to the intensity,
                  this functional derivative is independent of the intensity. We therefore
                  use this property as a cache.
    '''
    def __init__(self, *args, **kwargs):
        '''
        Dummy initialisation function (so that I can use this as a mixin)
        This just calls init_lightcurve.
        '''
        self.init_lightcurve(*args, **kwargs)

    def init_lightcurve(self, i, limb_darkening,
                 f=identity, df=np.ones_like, # function appplication
                 flux=None, σ_flux=None, ψ_flux=None, **kwargs):

        '''
        Initialise lightcurve inversion problem.
        '''

        self.i = i
        self.limb_darkening = limb_darkening

        self.f = f
        self.df = df

        assert flux is not None, "Must supply flux"
        assert flux.shape == σ_flux.shape and σ_flux.shape == ψ_flux.shape
        self.flux = flux
        self.σ_flux = σ_flux
        self.ψ_flux = ψ_flux

        self.f_flux = 1
        self.d_lightcurve = None

    @property
    def f_flux(self):
        '''
        Contribution to χ² function from lightcurve when used as mixin
        '''
        return self._f_flux

    @f_flux.setter
    def f_flux(self, f):
        self._f_flux = f

    @staticmethod
    def project_lightcurve(sph, ψ, i, limb_darkening=identity, f=identity):
        '''
        Generate a forward model of the light curve given a surface map, a set
        of rotational phases, and an inclination angle. Accepts a limb-darkening law,
        as well as (optionally) a function f so that for a surface map with values
        y, the local intensity is given as f(y).
        '''
        return np.array([
            f(sph).project(ψi, i, limb_darkening) for ψi in ψ
        ])

    @staticmethod
    def d_project_lightcurve(sph, ψ, i, limb_darkening=identity, f=identity, df=np.ones_like):
        '''
        Functional Derivative of forward model of the light curve in project_lightcurve. In addition
        to its arguments, accepts a function df(y) which returns df/dy.
        '''
        Δθ = np.median(np.diff(sph.θ.flatten()))
        derivative = np.array([
            df(sph) * sph.projectionmeasure(ψi, i, limb_darkening) for ψi in ψ
        ]) * Δθ * sph.Δφ
        return derivative

    def χ2_projection(self, sph):
        '''
        Projection vs observed values
        '''

        y = self.flux
        σ = self.σ_flux
        ψ = self.ψ_flux
        i = self.i
        f = self.f
        limb_darkening = self.limb_darkening

        model = self.project_lightcurve(sph, ψ, i, limb_darkening=limb_darkening, f=f)
        χ2 = np.sum(((model - y)/σ)**2)
        return χ2

    def grad_χ2_projection(self, sph):
        '''
        Gradient (functional derivative) of projection vs observed values
        '''

        y = self.flux
        σ = self.σ_flux
        ψ = self.ψ_flux
        i = self.i
        f = self.f
        df = self.df
        limb_darkening = self.limb_darkening

        model = self.project_lightcurve(sph, ψ, i, limb_darkening=limb_darkening, f=f)

        if self.d_lightcurve is not None:
            derivative = self.d_lightcurve
        else:
            derivative = self.d_project_lightcurve(sph, ψ, i,
                                                   limb_darkening=limb_darkening,
                                                   f=f, df=df)

        dχ2 = np.sum(
            [
                2 * (m - yi) / σi**2 * δm
                for m, yi, σi, δm in zip(model, y, σ, derivative)
            ]
        , axis=0)

        return dχ2

class CCFProblem:
    '''
    Container for CCF inversion cost functions, to be used as a mixin for
    the actual inversion class.

    Positional arguments:

    i: (float) inclination in RADIANS
    limb_darkening: (callable) Limb-darkening law as function of μ = cos ψ
    model_ccf: function F(v, T, vscale) that takes in the CCF velocity coordinate
               (in model units) and the local temperature,
               and returns a model CCF value.

    Keyword arguments:
    ccfs: list of ndarrays, each one containing the observed values of the CCF.
    e_ccfs: list of ndarrays of the same shape as ccfs, each one containing the
            estimated uncertainties in the observed CCF values
    v_grids: list of ndarrays of the same shape as ccfs, each one containing the
             grid of velocities on which the observed CCFs were evaluated.
    ψ_ccf: (ndarray) rotational phases associated with CCF observations. Must be
           same length as ccfs.
    vscale: conversion factor from mesh velocity coordinates (units of v_eq)
            to v_grid velocity coordinates (typically km/s).
    Ω: differential rotation (callable)

    In addition to these arguments, we define a few properties:

    f_ccf: relative weight of the CCF's contribution to the χ² function
           when used as a mixin
    ccf_scale: scale factors (either a scalar, or one per observation) to translate
               between model units and observed units of the CCF. We need this because
               we don't want to place incompatible constraints between the CCF problem
               and the lightcurve problem when the two are used together, so the
               normalisations for each CCF observation will be allowed to vary independently.
    '''

    def __init__(self, *args, **kwargs):
        '''
        Dummy initialisation function (so that I can use this as a mixin)
        This just calls init_ccf.
        '''
        self.init_ccf(*args, **kwargs)

    # caches for CCF problem

    _ccf_model = {}
    _d_ccf_model = {}

    def init_ccf(self, i, limb_darkening, model_ccf,
                     ccfs=None, e_ccfs=None, v_grids=None, ψ_ccf=None,
                     vscale=0, Ω=np.ones_like, **kwargs
                    ):

        '''
        Actually initialise the CCF inversion problem.
        '''
        self.i = i
        self.limb_darkening = limb_darkening
        self.Ω = Ω

        assert ccfs is not None, "Must supply CCFs"
        assert len(ccfs) == len(e_ccfs)
        assert len(e_ccfs) == len(v_grids)
        assert len(v_grids) == len(ψ_ccf)
        self.ccfs = ccfs
        self.e_ccfs = e_ccfs
        self.v_grids = v_grids
        self.ψ_ccf = ψ_ccf
        self.model_ccf = model_ccf
        self.ccf_scale = 1 # CCF normalisation scale(s)

        self.f_ccf = 1 # CCF contribution to total χ2
        self.vscale = vscale # turn units of vgrids into units accepted by model_ccf

    @property
    def f_ccf(self):
        '''
        Contribution to χ² function from CCF when used as mixin
        '''
        return self._f_ccf

    @f_ccf.setter
    def f_ccf(self, f):
        self._f_ccf = f

    @property
    def vscale(self):
        '''
        Conversion factor between units of vgrids and units accepted by self.model_ccf
        '''
        return self._vscale

    @vscale.setter
    def vscale(self, f):
        self._vscale = f

    @property
    def ccf_scale(self):
        '''
        Scale factor to reconcile forward modelled CCFs with observed values
        '''
        return self._ccf_scale

    @ccf_scale.setter
    def ccf_scale(self, f):
        self._ccf_scale = np.asarray(f) * np.ones(len(self.ccfs))

    @staticmethod
    def map_to_ccf(sph, i0, v_grids, ψ, vscale, limb_darkening=identity, Ω=np.ones_like):
        '''
        Produces CCF in units of v_eq, centred at the stellar rest frame,
        and normalised to unity.

        With an azimuthal grid of N mesh points, one recovers a smooth profile
        with vscale = 2π/N.

        Returns a list of length N_obs == len(ψ) == len(v_grids), with each element
        being an array of length len(v_grid[i]).
        '''
        assert len(ψ) == len(v_grids), "Must provide one v_grid for each ψ"
        assert isinstance(sph, CCFOblateSpheroid), "Must provide a guess of type CCFOblateSpheroid"

        return [sph.project_velocity(ψi, i0, v_grid, vscale,
                                     limb_darkening=limb_darkening, Ω=Ω)
                for ψi, v_grid in zip(ψ, v_grids)]

    @staticmethod
    def d_map_to_ccf(sph, i0, v_grids, ψ, vscale, limb_darkening=identity, Ω=np.ones_like):
        '''
        Functional derivative of above. For an OblateSpheroid of shape SHAPE, this produces
        a list (length N_obs) of numpy arrays, each of shape (N_V[i], *SHAPE),
        where N_V[i] == len(v_grids[i]).
        '''
        assert len(ψ) == len(v_grids), "Must provide one v_grid for each ψ"
        assert isinstance(sph, CCFOblateSpheroid), "Must provide a guess of type CCFOblateSpheroid"

        return [sph.velocity_projection_measure(ψi, i0, v_grid, vscale,
                                                limb_darkening=limb_darkening, Ω=Ω)
                for ψi, v_grid in zip(ψ, v_grids)]

    def renorm_ccf(self, sph, slices=None):
        '''
        Set scale factors for the individual CCF observations to be such that the mean ratio
        between the forward model and the observed CCFs is 1.
        '''
        if slices is None:
            slices = [slice(None, None)] * len(self.ccfs)
        self.ccf_scale = 1
        model = self.ccf_model(sph)
        self.ccf_scale = [1/np.mean((_ / t)[s]) for _, t, s in zip(model, self.ccfs, slices)]

    def ccf_model(self, sph):
        '''
        Generate a forward model of the CCFs, including scale factors for compatibility
        with observed CCFs.

        Returns a list of arrays containing CCFs models (one per CCF observation);
        each array in the list is of the same length as the observed CCF array.
        '''
        i = self.i
        vscale = self.vscale

        ψ = self.ψ_ccf
        vs = self.v_grids

        l = self.limb_darkening
        Ω = self.Ω

        if array_hash(sph) not in self._ccf_model:
            sph.ccf = self.model_ccf
            model = self.map_to_ccf(sph, i, vs, ψ, vscale, limb_darkening=l, Ω=Ω)
            self._ccf_model[array_hash(sph)] = model
        else:
            model = self._ccf_model[array_hash(sph)]

        return [s*m for s, m in zip(self.ccf_scale, model)]

    def χ2_ccf(self, sph):
        '''
        Cost function associated with forward models generated by self.ccf_model
        '''
        cs = self.ccfs
        es = self.e_ccfs

        model = self.ccf_model(sph)

        χ2 = np.nansum([np.nanmean(((m - c) / e)**2) for m, c, e in zip(model, cs, es)])

        return χ2

    def grad_χ2_ccf(self, sph):
        '''
        Functional derivative of cost function χ2_ccf with respect to the values of sph.
        Note that we must multiply the derivatives by the scale factors for consistency
        with self.ccf_model
        '''
        i = self.i
        vscale = self.vscale

        ψ = self.ψ_ccf
        vs = self.v_grids
        cs = self.ccfs
        es = self.e_ccfs

        l = self.limb_darkening
        Ω = self.Ω

        model = self.ccf_model(sph)

        if array_hash(sph) in self._d_ccf_model:
            derivatives = self._d_ccf_model[array_hash(sph)]
        else:
            sph.ccf = self.model_ccf
            derivatives = self.d_map_to_ccf(sph, i, vs, ψ, vscale, limb_darkening=l, Ω=Ω)
            # self._d_ccf_model[array_hash(sph)] = derivatives
            # This is commented out on purpose — for typical science workloads,
            # especially at EXPRES resolution, this turns out to be very memory-heavy
            # (about 1 GB per iteration of conjugate-gradient) and in any case
            # this appears to only be called once per iteration.

        derivatives = [s*d for s,d in zip(self.ccf_scale, derivatives)]

        dχ2 = np.sum([np.nanmean(2 * ((m - c) / e**2)[:,None,None] * d, axis=0)
                       for m, c, e, d in zip(model, cs, es, derivatives)], axis=0)

        return dχ2

    def pgrad_χ2_ccf(self, sph):
        '''
        Partial derivative of cost function with respect to ccf scale factors
        '''
        cs = self.ccfs
        es = self.e_ccfs

        model = self.ccf_model(sph)

        dχ2 = np.array([np.nanmean(2*m*(m - c) / e**2)
                        for m, c, e in zip(model, cs, es)])/self.ccf_scale

        return dχ2
