#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Various methods for interpolating and manipulating CCF models returned from
other codes. In particular, we provide a set of model CCFs that were included
with SOAP 2.0, as well as a set of CCFs derived from PHOENIX hi-res spectra
degraded with EXPRES resolution and instrumental throughput and computed with the
ESPRESSO K2 linelist (originally I made this for use with σ Gem).
'''

from os.path import dirname
import numpy as np
import scipy.interpolate as inter
from .oblate_spheroid import OblateSpheroid, δ, identity, v_r, μ

WHERE_AM_I = dirname(__file__)

def δ_ccf(L, y, v, Δv=1, dy=0):
    '''
    δ-function CCF model to match implementation in OblateSpheroid
    '''
    if dy == 1:
        return (1-δ(v, Δv)) * 0
    return (1-δ(v, Δv))

class CCFInterpolant:
    '''
    This class contains routines that simplify the interpolation of these CCF models
    given a standard data format: we expect a numpy pickle file containing a dict with
    the keys 'v' (velocity grid), as well as a set of numeric keys, each one corresponding
    to a temperature. The value of d['v'] should be a numpy array containing the CCF
    velocity grid points, and the other values of the dict should be numpy arrays of the
    same length, each one containing CCF values. By convention, we assume that the CCFs
    are all continuum-normalisd to unity.
    '''

    def __init__(self, path=None, **kwargs):
        if path is None:
            path = WHERE_AM_I + "/ccfs/SOAP/SOAP2.npy"
        data = np.load(path, allow_pickle=True)[()]
        self.v = data['v']
        self.keys = sorted([_ for _ in data.keys() if isinstance(_, (int, float))])
        self.data = data
        self.interpolant = inter.RectBivariateSpline(self.v, self.keys,
                                                     np.array([data[_] for _ in self.keys]).T,
                                                     **{'ky': min(len(self.keys)-1, 3), **kwargs})
    def __call__(self, *args, **kwargs):
        return self.interpolant(*args, **{'grid':False, **kwargs})

def interpolated_ccf_model(interp, y0, T0, v0):
    '''
    Given an interpolant, a standardised intensity scale y0,
    a standardised temperature scale T0, and a standardised
    velocity scale v0, return a suitable CCF model satisfying
    the following constraints:

    - The intensity y is related to a local temperature T as y ~ T^4, and
      T = T0 when y = y0.
    - The model returns the intensity-weighted CCF evaluated at a specified
      velocity v in units of v0, accounting for temperature shifts
      owing to limb-darkening (via the intensity factor L).
    - When we pass dy=1, we return the derivative of the CCF model with respect
      to y, even though the CCF model itself accepts a local temperature T.
    '''
    def model(L, y, v, Δv=v0, dy=0):
        T = (L * y / y0) ** (1/4) * T0
        if dy == 1:
            return T0*(L / y0 / y**3)**(1/4) * interp(v * v0, T, dy=1) / 4
        return interp(v * v0, T)

    return model


class CCFOblateSpheroid(OblateSpheroid):
    '''
    OblateSpheroid class, extended to accept arbitrary CCF models
    (set via the self.ccf property rather than with the constructor).
    The CCF model has a type signature f(y, v, Δv) where v is in model units,
    Δv is some characteristic velocity scale, and y is the local intensity.
    '''

    ccf = δ_ccf

    def project_velocity(self, φ0, i, v0, Δv,
        limb_darkening=identity, Ω=np.ones_like):

        '''
        Return projected intensity at a given radial velocity,
        given a velocity and velocity (full-)width.

        The function self.ccf is supposed to take care of intensity effects too — see
        implementation in the function δ_ccf
        '''

        L = limb_darkening(μ(self.θ, self.φ + φ0, self.ε, i))
        def integrate_single(v):
            return (self * self.ccf(L, self, v - v_r(self.θ, self.φ + φ0, self.ε, i, Ω=Ω), Δv, dy=0))\
                            .project(φ0, i, limb_darkening=limb_darkening)

        return np.vectorize(integrate_single)(v0)

    def velocity_projection_measure(self, φ0, i, v0, Δv,
                                    limb_darkening=identity, Ω=np.ones_like):

        r'''
        Despite the name, this is really a set of functional derivatives;
        we inherit the name from the implementation of OblateSpheroid where
        the two coincide.

        In particular, if the CCF at velocity vᵢ can be written as

        Cᵢ = \int dΩ μⱼ Lⱼ fⱼ m(vᵢ - vrⱼ, Lⱼ fⱼ) ~ \sum_j Mⱼ fⱼ m(vᵢ - vrⱼ, Lⱼ fⱼ),

        where vr_j is the local radial velocity of a mesh point, then
        the corresponding functional derivatives go as

        d Cᵢ / d fⱼ ~ Mⱼ  (m(vᵢ - vrⱼ, Lⱼ fⱼ) + fⱼ d/dfⱼ m(vᵢ - vrⱼ, Lⱼ fⱼ)).

        In particular if this model is equal to fⱼ * g(vᵢ - vrⱼ) then we simply recover
        μⱼ g(vᵢ - vrⱼ), which is precisely the (velocity) projection measure (as implemented
        in the OblateSpheroid class) as g → δ. However we require this more general case
        in order to use more sophisticated CCF models.

        This partial derivative is implemented by passing the keyword argument dy=1 to
        the callable function ccf, which is expected to be a property of the
        CCFOblateSpheroid object. This is intended to match the function signature of the
        BivariateSpline objects returned by scipy.interpolate.
        '''

        Δθ = np.median(np.diff(self.θ.flatten()))
        return np.array([self.velocity_projection_measure_single(φ0, i, v, Δv,
                                                                 limb_darkening=limb_darkening,
                                                                 Ω=Ω)
                          for v in v0]) * Δθ * self.Δφ

    def velocity_projection_measure_single(self, φ0, i, v0, Δv,
                                           limb_darkening=identity, Ω=np.ones_like):
        '''
        Functional derivative where v0 is expected to be a scalar.

        The product rule gives two parts: the pure CCF term (from differentiating
        the intensity weights) and the CCF partial derivative (from differentiating the
        CCF itself).
        '''

        L = limb_darkening(μ(self.θ, self.φ + φ0, self.ε, i))
        J = ( 
            self.ccf(L, self, v0 - v_r(self.θ, self.φ + φ0, self.ε, i, Ω=Ω), Δv, dy=0)
            + self * self.ccf(L, self, v0 - v_r(self.θ, self.φ + φ0, self.ε, i, Ω=Ω), Δv, dy=1)
        ) * self.projectionmeasure(φ0, i, limb_darkening=limb_darkening)

        return J
