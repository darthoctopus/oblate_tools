#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Data structure for scalar functions on oblate spheroids,
subclassing np.ndarray to permit automatic compatibility with ufuncs.

THE OBLATE SPHEROIDAL COORDINATE SYSTEM

Analogously to the spherical coordinate system, we define

x = ε sin θ cos φ
y = ε sin θ sin φ
z = cos θ

where θ is the colatitude and φ is the azimuth, and ε is the aspect
ratio of the ellipsoid. For oblateness, ε takes values between
0 (flat disk) and 1 (sphere). ε is related to the usual eccentricity
as e = sqrt(1 - ε^2).

For fixed ε, pulling back the metric on R3 yields the induced metric

g = (cos^2 θ + ε^2 sin θ) dθ^2 + sin^2 θ dφ^2.

In turn, this can be used as a definition for covariant derivatives etc.

We set up an observer coordinate system (ξ, η, ζ) via a rotation
around the x-axis by some inclination angle i (so that ξ === x),
choosing the ζ-axis of the observer coordinate system to be aligned
with the z-axis of the spheroid's coordinate system when i = 0, with
the (ξ, η) plane being the sky plane, and with the positive z direction
oriented towards the viewer. That is:

ξ = x
η = y cos i +  z sin i
ζ = -y sin i + z cos i

'''

import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import quad, simps

identity = lambda x: x

# global facts

def g11(θ, φ, ε):
    '''
    g11 = cos^2 θ + ε^2 sin θ
    '''
    return 1 + (ε**2 - 1) * np.sin(θ)**2

def g22(θ, φ, ε):
    '''
    g22 = sin^2 θ
    '''
    return np.sin(θ)**2

def sqrt_det_g(θ, φ, ε):
    '''
    det g = sin^2 θ (cos^2 θ + ε^2 sin^2 θ)
    '''
    return np.sqrt(g11(θ, φ, ε) * g22(θ, φ, ε))

def μ(θ, φ, ε, i):
    '''
    Return the limb-darkening argument μ (i.e. cosine of projected angle
    from line of sight ψ) given an inclination i and surface coordinates,
    for an observer at φ = -π/2.

    i=0 means that the polar axis and line of sight are aligned.

    This coordinate system is set up so that φ = 0 lies in the sky plane,
    and φ increases clockwise, going into the sky plane when i = π/2.

    μ = cos ψ = (cos θ cos i - ε sin θ sin φ sin i) / sqrt(cos^2 θ + ε^2 sin^2 θ)
    '''

    return ((np.cos(θ) * np.cos(i) - ε * np.sin(θ) * np.sin(i) * np.sin(φ))
            / np.sqrt(g11(θ, φ, ε)))

def θ_integral_limit(φ, ε, i):
    '''
    Return the iterated integral limit for θ as 0 <= θ <= θ0,
    assuming 0 <= i <= π/2.

    This is done by solving analytically for where μ = 0.
    '''

    return np.arctan2(np.cos(i), ε * np.sin(i) * np.sin(φ))

def projectionmeasure(θ, φ, ε, i):
    '''
    Return the projected integral measure
    sin θ (cos θ cos i - ε sin θ sin i sin φ)
    for an observer whose line of sight lies along φ = -π/2
    at an inclination i from the rotational axis.

    This is a dimensionless construction; the physical
    projected area can be recovered by multiplying by a^2,
    where a is the major axis length of the ellipsoid.

    Note that this is equal to μ * sqrt(det g), which can also
    be motivated purely from geometric considerations. Of course,
    this is a signed quantity (parts of the surface oriented
    away from the viewer take negative values). However, we
    can instead demand that parts of the star with μ <= 0 do
    not contribute to the integral (and so have projection
    measure of 0).
    '''
    m = μ(θ, φ, ε, i) * sqrt_det_g(θ, φ, ε)
    return np.where(m > 0, m, 0)

def v_r(θ, φ, ε, i, Ω=np.ones_like):
    '''
    Return the projected rotational radial velocity as seen by an
    observer whose line of sight is along φ = -π/2
    at an inclination i from the rotational axis.

    This is a dimensionless construction; the physical velocity
    can be recovered by multiplying by v_equator.

    Note that this doesn't depend on the ellipticity parameter.
    '''

    return np.sin(θ) * np.sin(i) * np.cos(φ) * Ω(θ)

def δ(v, Δv):
    '''
    Emulate a δ function with a Gaussian
    '''
    return 1/np.sqrt(1/2 * np.pi * (Δv)**2) * np.exp(-2 * (v)**2 / Δv**2)

class OblateSpheroid(np.ndarray):
    '''
    Data structure for scalar functions on an oblate spheroid

    Ideally, everything you're able to do with a numpy array should also be usable
    with this class (since I'm subclassing np.ndarray).
    '''
    def __new__(cls, data, θ, φ, ε, **kwargs):
        '''
        Initialises coordinate grid, requiring:
        θ: mesh points of colatitude (in the same sense as spherical coordinates)
        φ: mesh points of azimuth (in the same sense as spherical coordinates)
        ε: aspect ratio (b/a === tanh u)
        '''

        if len(np.asarray(data).shape) == 0:
            obj = (data * np.ones((len(np.asarray(θ).flatten()),
                                   len(np.asarray(φ).flatten())))).view(cls)
        else:
            obj = np.asarray(data, **kwargs).view(cls)

        obj.θ = np.asarray(θ).flatten()[:, None]
        obj.φ = np.asarray(φ).flatten()[None, :]
        obj.ε = float(ε)

        if obj.shape != (obj.θ.flatten().shape[0], obj.φ.flatten().shape[0]):
            print(obj.shape, (obj.θ.flatten().shape[0], obj.φ.flatten().shape[0]))
            raise ValueError("Data cannot be described as mesh over provided points")

        Δφ = np.mean(np.diff(φ))
        if not np.allclose(np.diff(φ), Δφ):
            raise ValueError("Azimuthal mesh has to be uniformly spaced")
        obj.Δφ = Δφ

        if ε == 1:
            obj.S = 4 * np.pi
        elif ε == 0:
            obj.S = 2 * np.pi
        else:
            k = np.sqrt(1/ε**2 - 1)
            obj.S = 4 * np.pi * (1 + ε * np.arcsinh(k)/k)/2

        return obj

    def __array_finalize__(self, obj):
        '''
        Required to subclass ndarray
        '''
        if obj is None:
            return
        self.θ = getattr(obj, 'θ', None)
        self.φ = getattr(obj, 'φ', None)
        self.Δφ = getattr(obj, 'Δφ', None)
        self.ε = getattr(obj, 'ε', None)
        self.S = getattr(obj, 'S', None)

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        '''
        Required to override ufunc evaluation as a subclass of ndarray.
        Will check if input spheroids are compatible.
        '''
        spheroids = [i for i in inputs if isinstance(i, OblateSpheroid)]
        flag = True
        for s1 in spheroids:
            for s2 in spheroids:
                if (s1.θ != s2.θ).any() or (s1.φ != s2.φ).any() or s1.ε != s2.ε:
                    flag = False
        if not flag:
            raise ValueError("All input spheroids have to share the same mesh and aspect ratio")

        args = [i.view(np.ndarray) if isinstance(i, OblateSpheroid) else i for i in inputs]
        results = super().__array_ufunc__(ufunc, method,
                                                              *args, **kwargs)
        if results is NotImplemented:
            return NotImplemented

        if ufunc.nout == 1:
            results = (results,)

        results = tuple((self.like(np.asarray(result))
                         if np.asarray(result).shape == self.shape else result)
                        for result in results)

        return results[0] if len(results) == 1 else results

    def __getitem__(self, item):
        '''
        Slices etc should NOT inherit properties like the coordinate mesh.
        This also exempts slices from mesh compatibility checks.
        '''
        return np.array(self)[item]

    @classmethod
    def ones(cls, N_θ, N_φ, ε, endpoint_θ=False):
        '''
        Convenience constructor to generate a coordinate mesh using np.linspace
        rather than having to specify grid values for θ and φ.

        Arguments:
        N_θ: number of θ values in the output mesh
        N_φ: number of φ values in the output mesh
        ε: ellipticity of the output mesh
        endpoint_θ: whether or not the coordinate mesh will include the singular points
                    at θ = 0 and θ = π.
        '''
        if endpoint_θ:
            θ = np.linspace(0, np.pi, N_θ)
        else:
            θ = np.linspace(0, np.pi, N_θ, endpoint=False)
            θ = θ + (θ[1] - θ[0])/2
        φ = np.linspace(0, 2 * np.pi, N_φ, endpoint=False)

        return cls(1, θ, φ, ε)

    def like(self, arr):
        '''
        If compatible, turn an ndarray into an OblateSpheroid with the same shape
        and coordinate mesh as a provided instance.
        '''

        if isinstance(arr, np.ndarray):
            assert arr.shape == self.shape,\
            "Shapes are not compatible for coercion to OblateSpheroid"
            return OblateSpheroid(np.asarray(arr), self.θ, self.φ, self.ε)
        raise NotImplementedError("I haven't decided how to implement this")

    def gradient(self):

        '''
        Covariant gradient of the function f.
        Note that the index has to be raised if a dot product is to be taken.
        '''
        d = np.nan_to_num(self[...])
        _ = self.like
        g_φ = d * 0
        g_φ[:, 1:-1] = (d[:, 2:] - d[:, :-2]) / 2 / self.Δφ
        g_φ[:, 0] = (d[:, 1] - d[:, -1]) / 2 / self.Δφ
        g_φ[:, -1] = (d[:, 0] - d[:, -2]) / 2 / self.Δφ

        return _(np.gradient(self[...], self.θ.flatten(), axis=0)), _(g_φ)

    def laplacian(self, simple=False):
        '''
        Scalar (note: NOT VECTOR) Laplacian operator.
        This is computed using Hodge duals as ★d★df.

        Hodge dual is taken using A = sqrt(g22/g11)
        and sqrt(det g), with
        g = (cos^2 θ + ε^2 sin^2 θ) dθ^2 + sin^2 θ dφ^2

        so 1/A = sqrt(cot^2 θ + ε^2), and
        det g = sin^2 θ (cos^2 θ + ε^2 sin^2 θ)

        For best results, use a grid where θ goes
        from (ε, π - ε) rather than (0, π)
        '''

        # I have to implement my own second difference scheme because
        # using gradient() twice results in the image being segmented into
        # two independent subspaces when using the Laplacian for Tikhonov
        # regularisation, producing a characteristic dithering pattern.

        def d2φ(f):
            d = np.asarray(f)
            g = np.copy(d)
            g[:, 1:-1] = (d[:, 2:] + d[:, :-2] - 2 * d[:, 1:-1]) / self.Δφ ** 2
            g[:, 0] = (d[:, 1] + d[:, -1] - 2 * d[:, 0]) / self.Δφ ** 2
            g[:, -1] = (d[:, 0] + d[:, -2] - 2 * d[:, -1]) / self.Δφ ** 2
            return g

        Δθ = np.median(np.diff(self.θ.flatten()))

        def d2θ(f):
            d = np.asarray(f)
            g = np.copy(d)
            g[1:-1, :] = (d[2:] + d[:-2] - 2 * d[1:-1]) / Δθ**2
            g[0] = g[1] # correct to O(Δθ^3)
            g[-1] = g[-2]
            return g

        A = np.sqrt(g22(self.θ, self.φ, self.ε)/g11(self.θ, self.φ, self.ε))
        Aθ = np.gradient(A, self.θ.flatten(), axis=0) # the metric is azimuthally symmetric

        pθ, pφ = self.gradient()
        if simple:
            Hθ, Hφ = pθ * A, pφ / A #Hodge duals, modulo sign
            HL = Hθ.gradient()[0] + Hφ.gradient()[1]
        else:
            HL = ((d2θ(self) * A + pθ * Aθ) + (d2φ(self) / A))

        L = HL / sqrt_det_g(self.θ, self.φ, self.ε)
        return L

    def mod_square_gradient(self):
        '''
        Mod square of the gradient: g(df♯, df♯).
        (dividing rather than multiplying by metric
        components because this is covariant gradient)
        '''
        grad = self.gradient()
        return grad[0]**2 / g11(self.θ, self.φ, self.ε) + grad[1]**2 / g22(self.θ, self.φ, self.ε)

    def projectionmeasure(self, φ0, i, limb_darkening=identity):
        '''
        Integral measure of contribution to intensity integral given a rotational
        phase and inclination
        '''
        return projectionmeasure(self.θ, self.φ + φ0, self.ε, i)\
              * limb_darkening(μ(self.θ, self.φ + φ0, self.ε, i))

    def project(self, φ0, i, limb_darkening=identity, method='quick', kind='cubic',
                fill_value='extrapolate', **kwargs):
        '''
        return projected integral, given an inclination i
        and a rotational phase φ0.
        '''

        if method == 'quick':
            Δθ = np.median(np.diff(self.θ.flatten()))
            return np.nansum(self * self.projectionmeasure(φ0, i, limb_darkening)) * Δθ * self.Δφ
        I_θ = []
        for j, φ in enumerate(self.φ.flatten()):
            θ0 = θ_integral_limit(φ + φ0, self.ε, i)
            meridian = self[:, j]
            interpolant = interp1d(self.θ.flatten(), meridian,
                                   kind=kind, fill_value=fill_value, **kwargs)
            integrand = lambda θ: interpolant(θ) * projectionmeasure(θ, φ + φ0, self.ε, i)\
                                  * limb_darkening(μ(θ, φ + φ0, self.ε, i))
            I_θ.append(quad(integrand, 0, θ0))

        return np.nansum(I_θ) * self.Δφ

    def surfacemeasure(self):
        '''
        Surface integral measure (= sqrt(det(g)))
        '''
        return sqrt_det_g(self.θ, self.φ, self.ε)

    def surface_integrate(self, method='quick', kind='cubic', fill_value='extrapolate', **kwargs):
        '''
        Perform a surface integral with respect to the
        measure sqrt(det(g))
        '''
        if method == 'quick':
            Δθ = np.median(np.diff(self.θ.flatten()))
            return np.nansum(self * self.surfacemeasure()) * Δθ * self.Δφ
        I_θ = []
        for j, φ in enumerate(self.φ.flatten()):
            meridian = self[:, j]
            if method == 'quad':
                interpolant = interp1d(self.θ.flatten(), meridian,
                                       kind=kind, fill_value=fill_value, **kwargs)
                integrand = lambda θ: interpolant(θ) * sqrt_det_g(θ, φ, self.ε)
                I_θ.append(quad(integrand, np.min(self.θ), np.max(self.θ)))
            else:
                integrand = meridian * sqrt_det_g(self.θ.flatten(), φ, self.ε)
                I_θ.append(simps(integrand, self.θ.flatten()))
        return np.nansum(I_θ) * self.Δφ

    def tikhonov(self, **kwargs):
        '''
        Tikhonov cost function: surface integral of |grad f|^2
        '''
        return self.mod_square_gradient().surface_integrate(**kwargs)

    def project_velocity(self, φ0, i, v0, Δv,
        limb_darkening=identity, Ω=np.ones_like):
        '''
        Return projected intensity at a given radial velocity,
        given a velocity and velocity (full-)width.
        '''
        def integrate_single(v):
            return (self * δ(v - v_r(self.θ, self.φ + φ0, self.ε, i, Ω=Ω), Δv))\
                    .project(φ0, i, limb_darkening=limb_darkening)

        return np.vectorize(integrate_single)(v0)

    def velocity_projection_measure(self, φ0, i, v0, Δv,
                                    limb_darkening=identity, Ω=np.ones_like):
        '''
        Velocity projection measure associated with an array of radial velocities v0.
        '''
        Δθ = np.median(np.diff(self.θ.flatten()))
        return np.array([self.velocity_projection_measure_single(φ0, i, v, Δv,
                                                                 limb_darkening=limb_darkening,
                                                                 Ω=Ω)
                         for v in v0]) * Δθ * self.Δφ

    def velocity_projection_measure_single(self, φ0, i, v0, Δv,
                                           limb_darkening=identity, Ω=np.ones_like):
        '''
        Velocity projection measure where v0 is expected to be a scalar
        '''
        return δ(v0 - v_r(self.θ, self.φ + φ0, self.ε, i, Ω=Ω), Δv)\
            * self.projectionmeasure(φ0, i, limb_darkening=limb_darkening)
