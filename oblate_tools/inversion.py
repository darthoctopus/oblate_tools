#!/usr/bin/env python3

## Author: Joel Ong <joel.ong@yale.edu>
## Yale University Dept. of Astronomy

'''
Inversion code including various regularisers
'''

import numpy as np
from .oblate_spheroid import identity, OblateSpheroid
from .ccf import CCFOblateSpheroid
from .costs import LightcurveProblem, CCFProblem, tqdm_minimize

class CachedOblateSpheroid(CCFOblateSpheroid):
    '''
    OblateSpheroid class with many important (computationally-intensive,
    repeatedly-used, but otherwise invariant) quantities precached.
    '''

    # initialise caches

    _projectionmeasure = {}
    # _velocity_projection_measure_single = {}
    # _model = None

    def projectionmeasure(self, *args, **kwargs):
        '''
        Returns memoised values if possible
        '''
        if args not in self._projectionmeasure:
            self._projectionmeasure[args] = super().projectionmeasure(*args, **kwargs)
        return self._projectionmeasure[args]

    # def velocity_projection_measure_single(self, *args, **kwargs):
    #     if args not in self._velocity_projection_measure_single:
    #         self._velocity_projection_measure_single[args] = \
    #              super().velocity_projection_measure_single(*args, **kwargs)
    #     return self._velocity_projection_measure_single[args]

    def like(self, arr, *_, **__):
        '''
        If compatible, turn an ndarray into a CachedOblateSpheroid with the
        same shape, inheriting caches (since we pass dicts by reference).
        '''

        if isinstance(arr, np.ndarray):
            assert arr.shape == self.shape,\
            "Shapes are not compatible for coercion to OblateSpheroid"
            out = CachedOblateSpheroid(np.asarray(arr), self.θ, self.φ, self.ε)
            out._projectionmeasure = self._projectionmeasure
            out.ccf = self.ccf
            # out._velocity_projection_measure_single = self._velocity_projection_measure_single
            return out
        raise NotImplementedError("Haven't figured this one out yet")

    @classmethod
    def cache(cls, obs):
        '''
        Convert an OblateSpheroid to a CachedOblateSpheroid with the same
        shape and properties.
        '''
        if isinstance(obs, CCFOblateSpheroid):
            q = cls(obs.data, obs.θ, obs.φ, obs.ε)
            q.ccf = obs.ccf
            return q
        if isinstance(obs, OblateSpheroid):
            return cls(obs.data, obs.θ, obs.φ, obs.ε)
        raise NotImplementedError


class InversionProblem(LightcurveProblem, CCFProblem):
    '''
    Wrapper for most inversion-related things.

    Once the object has been instantiated, it is callable
    (with an initial guess on a compatible OblateSpheroid passed
    as an argument).

    Example usage:
    ```
    prob = InversionProblem(init * 0, i0, limb_darken_σ_Gem,
                            flux=flux, σ_flux=σ, ψ_flux=φ0, λ_tikhonov=1e-4)
    prob.λ_tikhonov = 0
    q = prob(init)
    ```

    Arguments:

    mesh: (OblateSpheroid) mesh for optimisation. Actual values don't matter.
    i: (float) inclination in RADIANS
    limb_darkening: (callable) Limb-darkening law as function of μ = cos ψ

    Keyword arguments:
    ccfs: continuum-normalised CCFs (NOTE: DIFFERENT FROM `CCFProblem`)
    e_ccfs: continuum-normalised CCF errors (NOTE: DIFFERENT FROM `CCFProblem`)

    Properties:
    λ_tikhonov: (float) size of Tikhonov regularisation term
    λ_max_S: (float) size of max-entropy regularisation term
    λ_ΔM: (float) size of mean-difference regularisation term

    f_flux, f_ccf: weighting coefficients for different χ² terms

    Also see documentation for LightcurveProblem and CCFProblem
    for the respective arguments required in order to include lightcurve
    and CCF constraints in the inversion problem.
    '''
    def __init__(self, mesh, i, limb_darkening, *,
                 f=identity, df=np.ones_like, # function appplication
                 **kwargs
                ):

        self.mesh = CachedOblateSpheroid(mesh, mesh.θ, mesh.φ, mesh.ε)

        # We don't necessarily require that the input mesh be of type
        # CCFOblateSpheroid

        self.mesh.ccf = getattr(mesh, 'ccf', None)
        self.N = np.prod(self.mesh.shape)

        self.λ_tikhonov = 0
        self.λ_max_S = 0
        self.λ_ΔM = 0

        self.f_flux = 1
        self.f_ccf = 1
        self.f_ccf_scale = 0

        self.costs = {}

        def cast(x):
            if isinstance(x, np.ndarray):
                return self.mesh.like(np.asarray(x).reshape(-1)[:self.N].reshape(mesh.shape))
            raise NotImplementedError

        if 'flux' in kwargs:
            self.init_lightcurve(i, limb_darkening, **kwargs)
            self.d_lightcurve = self.d_project_lightcurve(mesh, self.ψ_flux, i,
                                                         limb_darkening=limb_darkening,
                                                         f=f, df=df)

        if 'ccfs' in kwargs:

            if 'model_ccf' not in kwargs:
                kwargs['model_ccf'] = self.mesh.ccf

            self.init_ccf(i, limb_darkening, **kwargs)

            # input CCFs are assumed to be scaled to unit continuum normalisation

            self.raw_ccfs = kwargs['ccfs']
            self.raw_e_ccfs = kwargs['e_ccfs']

        def cost(sph):
            '''
            Raw cost function associated with just the intensity field
            '''
            acc = 0
            if 'flux' in kwargs and self.f_flux > 0:
                self.costs['χ2_projection'] = self.χ2_projection(sph)
                acc += self.costs['χ2_projection'] * self.f_flux
            if 'ccfs' in kwargs and self.f_ccf > 0:
                self.costs['χ2_ccf'] = self.χ2_ccf(sph)
                acc += self.costs['χ2_ccf'] * self.f_ccf
            if self.λ_tikhonov > 0:
                self.costs['tikh'] = np.nan_to_num((self.T(sph)))
                acc += self.λ_tikhonov * self.costs['tikh']
            if self.λ_max_S > 0:
                acc += self.λ_max_S * np.nan_to_num((self.S(sph)))
            if self.λ_ΔM > 0:
                acc += self.λ_ΔM * np.nan_to_num((self.ΔM(sph)))
            return acc

        def gradcost(sph):
            '''
            Functional derivatives associated with just the intensity field
            '''
            acc = 0
            if 'flux' in kwargs and self.f_flux > 0:
                acc += self.grad_χ2_projection(sph) * self.f_flux
            if 'ccfs' in kwargs and self.f_ccf > 0:
                acc += self.grad_χ2_ccf(sph) * self.f_ccf
            if self.λ_tikhonov > 0:
                acc += self.λ_tikhonov * np.nan_to_num((self.grad_T(sph)))
            if self.λ_max_S > 0:
                acc += self.λ_max_S * np.nan_to_num((self.grad_S(sph)))
            if self.λ_ΔM > 0:
                acc += self.λ_ΔM * np.nan_to_num((self.grad_ΔM(sph)))
            return acc

        def paramcost(sph, params):
            if 'ccfs' in kwargs and self.f_ccf > 0 and self.f_ccf_scale > 0:
                self.ccf_scale = params
            return 0

        def paramgradcost(sph, params):
            if 'ccfs' in kwargs and self.f_ccf > 0 and self.f_ccf_scale > 0:
                try:
                    self.ccf_scale = params
                    return self.pgrad_χ2_ccf(sph) * self.f_ccf * self.f_ccf_scale
                except (ValueError, AttributeError):
                    raise
            return np.zeros_like(params)

        def embellish(sph, params):
            return np.concatenate((sph.reshape(-1), params))

        def lift(sph):
            '''
            Lift a pure OblateSpheroid to the embellished category
            '''
            acc = []
            if 'ccfs' in kwargs and self.f_ccf > 0 and self.f_ccf_scale > 0:
                acc = np.concatenate((acc, self.ccf_scale))
            return embellish(sph, acc)

        def project(x):
            '''
            Project from the embellished category back to the product space of the intensity
            field and other parameters.
            '''
            y = x.reshape(-1)

            sph = cast(y[:self.N])
            params = y[self.N:]

            return sph, params

        def g(x):
            '''
            x is assumed to be some embellished quantity: the first N elements
            specify the intensity field, and the remainder specify additional parameters
            '''
            sph, params = project(x)

            return paramcost(sph, params) + cost(sph)

        def jac(x):
            '''
            As above, this is assumed to be some embellished quantity.
            '''
            sph, params = project(x)

            jac = gradcost(sph)[...]
            pjac = paramgradcost(sph, params)

            return embellish(jac, pjac)

        self.g = g
        self.jac = jac
        self.lift = lift
        self.project = project
        self._ = lambda x: project(x)[0]

    def __call__(self, init, **kwargs):
        '''
        init is assumed to be an OblateSpheroid
        '''
        return tqdm_minimize(self.g, self.lift(init), jac=self.jac, **kwargs)

    @property
    def λ_tikhonov(self):
        '''
        Weight of Tikhonov regularisation term
        '''
        return self._λ_t

    @λ_tikhonov.setter
    def λ_tikhonov(self, λ):
        self._λ_t = λ

    @property
    def λ_max_S(self):
        '''
        Weight of entropy-measure regularisation term
        '''
        return self._λ_S

    @λ_max_S.setter
    def λ_max_S(self, λ):
        self._λ_S = λ

    @property
    def λ_ΔM(self):
        '''
        Weight of mean-deviation regularisation term
        '''
        return self._λ_M

    @λ_ΔM.setter
    def λ_ΔM(self, λ):
        self._λ_M = λ

    @staticmethod
    def T(sph):
        '''
        Tikhonov cost function
        '''
        return sph.tikhonov()

    @staticmethod
    def grad_T(sph):
        '''
        Gradient of Tikhonov cost function
        '''
        Δθ = np.median(np.diff(sph.θ.flatten()))
        return -2 * sph.laplacian() * sph.surfacemeasure() * Δθ * sph.Δφ

    @staticmethod
    def S(sph):
        '''
        Entropy measure
        '''
        return (- sph * np.log(sph)).surface_integrate()

    @staticmethod
    def grad_S(sph):
        '''
        Gradient of Entropy measure
        '''
        Δθ = np.median(np.diff(sph.θ.flatten()))
        return (- np.log(sph) - 1) * sph.surfacemeasure() * Δθ * sph.Δφ

    @staticmethod
    def ΔM(sph):
        '''
        Deviation from mean
        '''
        M = sph.surface_integrate() / sph.S
        return ((sph - M)**2).surface_integrate()

    @staticmethod
    def grad_ΔM(sph):
        '''
        Gradient of Deviation from mean
        '''
        Δθ = np.median(np.diff(sph.θ.flatten()))
        M = sph.surface_integrate() / sph.S
        μ = sph.surfacemeasure() * Δθ * sph.Δφ
        δΔM = 2 * (sph - M) * μ
        return δΔM * (1 - μ / sph.S)
