import numpy as np
import matplotlib.pyplot as plt

from oblate_tools import show
from oblate_tools.science import limb_darken_power_2
from oblate_tools.io.harmon import read_txt, phase_to_viewport_φ, map_to_profile

def _(degrees):
    '''
    Degrees to radians
    '''
    return np.array(degrees) / 360 * 2 * np.pi

# TESTING
if __name__ == '__main__':

    WIDTH = 640
    HEIGHT = 320

    θ = np.linspace(1e-2, np.pi-1e-2, HEIGHT)
    φ = np.linspace(0, 2 * np.pi, WIDTH, endpoint=False)

    o = read_txt("test.txt", θ, φ)
    v_grid = np.linspace(-1, 1, 50)
    i0 = float(_(72.3))
    for phase in [0, .25, .5, .75]:
        φ0 = phase_to_viewport_φ(phase)
        try:
            show(o, cmap='inferno', i0=i0, limb_darkening=limb_darken_power_2,
             darken=True, φ0=φ0, save=f"phase_{phase:.2f}_face.png")
        except RuntimeError:
            pass
        
        lineprofile = 1-map_to_profile(o, φ0, i0, v_grid)
        plt.plot(v_grid, lineprofile)
        plt.savefig(f"phase_{phase:.2f}_profile.png")
        plt.close('all')
